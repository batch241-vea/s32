let http = require('http');

let users = [
{
	"name": "Jose Marie Chan",
	"email": "jmarie@mail.com"
},
{
	"name": "Mariah Carey",
	"email": "mariahcarey@mail.com"
}
];

let port = 8080;

let server = http.createServer(function(request, response){

	if(request.url == '/users' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(JSON.stringify(users))
		response.end();
	} else if (request.url == '/users' && request.method == 'POST') {
		let request_body = '';

		request.on('data', function(data){
			request_body += data
			console.log(request_body)
		})

		request.on('end', function(){
			console.log(typeof request_body)


			request_body = JSON.parse(request_body);

			let new_user = {
				"name": request_body.name,
				"email": request_body.email
			}

			users.push(new_user);
			console.log(users);

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(new_user))
			response.end()

		})
	}
});


server.listen(port);
console.log(`Server is running at localhost: ${port}`);
